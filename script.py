#!/bin/env python

import sys
import json
import yt_dlp


from flask import Flask, request, redirect

app = Flask(__name__)


@app.route('/')
def hello():
    return 'Hello, World!'

@app.route("/r", methods=["GET"])
def extract_video_url():
    url = request.args.get('url', default=None)
    level = request.args.get('l', default=None)
    format_id = request.args.get('f', default=None)

    if url:
        # ℹ️ See help(yt_dlp.YoutubeDL) for a list of available options and public functions
        ydl_opts = {'verbose': False}
        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            info = ydl.extract_info(url, download=False)

            # ℹ️ ydl.sanitize_info makes the info json-serializable
            # print(json.dumps(ydl.sanitize_info(info)))
            sinfo = ydl.sanitize_info(info)

            if level:
                if level == "1":
                    return json.dumps(sinfo)
                else:
                    return "Level: {}".format(level)
            else:
                if format_id:
                    for formats in sinfo["formats"]:
                        if formats["format_id"] == str(format_id):
                            return redirect(formats["url"], code=302)
                    else:
                        return "Format: {} not found".format(format_id)
                else:
                    if sinfo:
                        return redirect(sinfo["formats"][0]["url"], code=302)
                    else:
                        return "Unable to process your request"

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=6543, debug=False)
